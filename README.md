# OneSock
A shared directory for android devices.

## Infrastructure
The app will be written in Kotlin. Developed in Android Studio.

The server will run on Google's Firebase.

## Design
### Networking
#### Model
This section explains how I chose to implement the shared directory. In 
my shared directory model, only files are shared; empty directories 
created by users will not be created for the other users, until a file 
is uploaded into them.

##### Action
The model is based on *actions*. An action consists of a file path, a 
timestamp (epoch time) and the state of that file; for example, an 
action representing a new file might look like this: 
`Action(ActionType.NEW, 3123124, "/some/new/file")`

##### Client
For each file in the shared directory, the client saves the last action 
made on it. For example whenever a file is uploaded, an ADD action is 
saved for it with the current timestamp. The client saves the action on 
the disk.

The client also saves, for each file, a `SyncStatus`, which is logically 
just SYNCED, UNTRACKED (for directories) or NOT_SYNCED (NOT_SYNCED is 
divided to TO_BE_ADDED and TO_BE_DELETED but for user expirience only, 
this does not affect the model).

##### Server
The server, very much like the clients, also has its own directory with 
files and the last action each file has. The directory in the server is 
always updated from clients.

##### Resolving
A sync session is ensued from clients whenever one of the following 
events occur:
* A new action is added (new file, file deleted)
* The application starts
* The server's state was changed (another client finished a session 
against the server)

Sync sessions occur only when internet is available.

When a client connects to the server to start a sync session, it first 
reads all the actions from the server.

It then resolves the actions from the server against its own. If an 
action from the server is newer then a local action for a certain file, 
that action is executed; for example, if the server has 
`Action(ActionType.ADD, 300, "/some/file")` and the local action for 
that file is `Action(ActionType.ADD, 210, "some/file")` then the action 
from the server will be executed - the file will be downloaded and will 
replace the current file.

It then resolves the local actions against the server's the same way; if 
a local action for a file is newer than the server's for that file, the 
action is executed on the server. For example, if the server has 
`Action(ActionType.ADD, 300, "/some/file")` and the local action for 
that file is `Action(ActionType.DELETE, 340, "some/file")` then the 
action from the client will be executed - the file will be deleted on 
the server.

#### User
To upload/update files, the user chooses a file from the storage on 
his/her device to upload. It will be created in the directory currently 
opened.

To create a directory, the user clicks on a button and chooses a name 
for the new directory.

To delete a file, the user clicks-and-holds his/her finger on a file.

During a sync session, a sync icon will be displayed, and the user won't 
be able to upload or delete files.

After uploading a file, the file will be marked as `to-be-uploaded`. After 
the file is successfully uploaded to the server, the file is marked as 
`synced`.

When the user deletes a file, it will be marked as `to-be-deleted` until 
the server is notified about the deletion, and then it will just 
disappear from the directory.

While uploading a file, it will be marked with `uploading {upload percentage}` and when 
downloading a file, with `downloading {download percentage}`.

### Future features
* Deleting files from the server once all users are updated about that 
file
* Users and groups - currently there is only one directory for all of 
the users of the app, since it's just in the POC stage
* Adding a new client to an existing shared directory.
* Local Wifi Sync - A way to sync 2 clients without the server. The 
clients will transfer the resource from one to the other, and will have 
to notify the server of the transfer so it will be able to keep tracking 
resources. Useful when adding a new client to an existing shared 
directory.
* Smart Server Storage - The way the server handles its storage may be 
improved to account for relevance, for example if one client is offline 
for quite some time, the server deletes resources that only that client 
needs, and when that client connects again the server will treat the 
missing resources as deleted and receive them from other clients again.
* Direct Peer-To-Peer Sync - When 2 clients are connected to the server 
at the same time, and one clients needs some resources from another, 
then the server will lead them to create their own peer-to-peer 
connection and transfer the files directly, without having network and 
storage limitations from the storage.
* File Changes - The shared directory will not only send new files 
between clients, but will also sync changes much like git. The model 
might even use git itself for tracking file changes.
