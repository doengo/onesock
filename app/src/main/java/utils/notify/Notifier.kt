package utils.notify

abstract class Notifier<T> {
    private val notifiedList: MutableList<Notified<T>> = ArrayList()

    fun register(notified: Notified<T>) {
        notifiedList.add(notified)
    }

    fun unregister(notified: Notified<T>) {
        notifiedList.remove(notified)
    }

    fun notify(argument: T) {
        for (notified in notifiedList) {
            notified.onNotify(argument)
        }
    }
}

interface Notified<T> {
    fun onNotify(argument: T)
}