package utils.general

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.provider.MediaStore
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.ImageView
import com.tal.ido.onesock.R
import com.tal.ido.onesock.data.Cache
import java.io.File

const val LOG_TAG = "OneSock"
var baseDirectoryPath: String = ""

fun setBaseDirectory(path: String) {
    baseDirectoryPath = path
}

fun getRelativePath(absolutePath: String): String {
    return absolutePath.substring(baseDirectoryPath.length)
}

fun getAbsolutePath(relativePath: String): String {
    return baseDirectoryPath + relativePath
}

val ESCAPING_CHARACTERS = charArrayOf('%', '^', '~', '{', '}', '`')

fun toEscapedFirebaseDatabasePath(relativePath: String): String {
    return relativePath
            .replace('.', '%')
            .replace('#', '^')
            .replace('$', '~')
            .replace('[', '{')
            .replace(']', '}')
            .replace('/', '`').substring(1)
}

fun fromEscapedFirebaseDatabasePath(firebasePath: String): String {
    return "/" + firebasePath
            .replace('%', '.')
            .replace('^', '#')
            .replace('~', '$')
            .replace('{', '[')
            .replace('}', ']')
            .replace('`', '/')
}

fun escapableFileName(fileName: String): Boolean {
    for (c in ESCAPING_CHARACTERS)
        if (fileName.contains(c)) {
            return false
        }
    return true
}

fun setThumbnail(file: File, fileIcon: ImageView, videoTopIcon: ImageView, activity: Activity, thumbnailsCache: Cache<String, Bitmap>) {
    val ext = file.name.substring(file.name.lastIndexOf(".") + 1)

    videoTopIcon.visibility = when (ext) {
        "mp4" -> VISIBLE
        else -> INVISIBLE
    }

    if (file.isDirectory) {
        fileIcon.setImageResource(R.drawable.dir_icon)
        return
    } else {
        if (ext == "mp3") {
            fileIcon.setImageResource(R.drawable.audio_icon)
        } else {
            fileIcon.setImageResource(R.drawable.file_icon)
        }
    }

    Thread(Runnable {
        calculateAndSetHeavyThumbnail(ext, file, activity, fileIcon, thumbnailsCache)
    }).start()
}

private fun calculateAndSetHeavyThumbnail(ext: String, file: File, activity: Activity, fileIcon: ImageView, thumbnailsCache: Cache<String, Bitmap>) {
    val thumbnail = when (ext) {
        "mp4" -> ThumbnailUtils.createVideoThumbnail(file.absolutePath, MediaStore.Video.Thumbnails.MINI_KIND)
        "png" -> ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.absolutePath), 96, 96)
        else -> return
    } ?: return

    thumbnailsCache.put(file.absolutePath, thumbnail)
    activity.runOnUiThread {
        fileIcon.setImageBitmap(thumbnail)
    }
}