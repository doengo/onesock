package utils.activity

import android.support.v7.app.AppCompatActivity

abstract class BackGoingActivity : AppCompatActivity() {
    abstract fun goBack()
}