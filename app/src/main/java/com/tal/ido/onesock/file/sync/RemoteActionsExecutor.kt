package com.tal.ido.onesock.file.sync

import android.util.Log
import com.google.firebase.storage.FileDownloadTask
import utils.general.LOG_TAG
import com.tal.ido.onesock.action.Action
import com.tal.ido.onesock.action.ActionType
import com.tal.ido.onesock.action.ActionsRepository
import com.tal.ido.onesock.file.sync.status.FileSyncStatus
import com.tal.ido.onesock.file.sync.status.FileSyncStatusType
import com.tal.ido.onesock.file.sync.status.StatusRepository
import utils.general.getAbsolutePath
import java.io.File

class RemoteActionsExecutor(private val actionsRepository: ActionsRepository, private val statusRepository: StatusRepository, private val firebase: FirebaseFacade, private val syncGui: SyncGui) {
    fun executeActionFromServer(databaseAction: Action, downloadTasks: ArrayList<FileDownloadTask>) {
        val relativePath = databaseAction.relativeFilePath
        val absolutePath = getAbsolutePath(relativePath)

        when (databaseAction.type) {
            ActionType.ADD -> {
                addFileFromServer(relativePath, absolutePath, databaseAction, downloadTasks)
            }
            ActionType.DELETE -> {
                deleteFileFromServer(relativePath, databaseAction, absolutePath)
            }
        }
    }

    private fun addFileFromServer(relativePath: String, absolutePath: String, databaseAction: Action, downloadTasks: ArrayList<FileDownloadTask>) {
        statusRepository.set(FileSyncStatus(FileSyncStatusType.DOWNLOADING), relativePath)
        val localFile = File(absolutePath)

        localFile.parentFile.mkdirs()
        localFile.createNewFile()
        syncGui.onDownloadStart(relativePath)
        val downloadTask = firebase.downloadFile(relativePath, absolutePath)

        downloadTask.addOnSuccessListener {
            syncGui.onDownloadedFile(relativePath)
            statusRepository.set(FileSyncStatus(FileSyncStatusType.SYNCED), relativePath)
            actionsRepository.addAction(databaseAction)
        }.addOnFailureListener { e ->
            Log.e(LOG_TAG, "Failed to download file $relativePath", e)
            statusRepository.set(FileSyncStatus(FileSyncStatusType.DOWNLOADING, 0), relativePath)
        }.addOnProgressListener { taskSnapshot ->
            statusRepository.set(FileSyncStatus(FileSyncStatusType.DOWNLOADING, ((100.0 * taskSnapshot.bytesTransferred)/taskSnapshot.totalByteCount).toInt()), relativePath)
        }

        downloadTasks.add(downloadTask)
    }

    private fun deleteFileFromServer(relativePath: String, databaseAction: Action, absolutePath: String) {
        statusRepository.set(FileSyncStatus(FileSyncStatusType.SYNCED), relativePath)
        actionsRepository.addAction(databaseAction)

        val file = File(absolutePath)
        if (file.exists()) {
            file.delete()
        }
    }
}