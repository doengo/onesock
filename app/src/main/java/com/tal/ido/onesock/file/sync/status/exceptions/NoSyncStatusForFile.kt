package com.tal.ido.onesock.file.sync.status.exceptions

class NoSyncStatusForFile(message: String) : Exception(message)
