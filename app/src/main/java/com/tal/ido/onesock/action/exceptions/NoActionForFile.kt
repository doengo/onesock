package com.tal.ido.onesock.action.exceptions

import java.lang.Exception

class NoActionForFile(relativePath: String) : Exception(relativePath)
