package com.tal.ido.onesock.action

enum class ActionType(val value: Int) {
    ADD(0),
    DELETE(2)
}
