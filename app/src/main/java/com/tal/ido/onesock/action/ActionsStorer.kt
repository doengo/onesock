package com.tal.ido.onesock.action

import android.content.SharedPreferences
import android.util.Base64
import com.tal.ido.onesock.action.exceptions.NoActionForFile
import java.io.*


class ActionsStorer(private val actionsPreferences: SharedPreferences) {
    fun writeAction(action: Action) {
        val actionBytesOutputStream = ByteArrayOutputStream()
        val objectOutputStream = ObjectOutputStream(actionBytesOutputStream)
        objectOutputStream.writeObject(action)
        objectOutputStream.close()
        val actionBytes = actionBytesOutputStream.toByteArray()
        val encodedActionBytes = Base64.encodeToString(actionBytes, Base64.DEFAULT)
        actionsPreferences.edit().putString(action.relativeFilePath, encodedActionBytes).apply()
    }

    fun readAction(relativePath: String): Action {
        val encodedActionBytes = actionsPreferences.getString(relativePath, "none")
        if (encodedActionBytes == "none") {
            throw NoActionForFile(relativePath)
        }
        return readActionFromEncodedBytes(encodedActionBytes)
    }

    private fun readActionFromEncodedBytes(encodedActionBytes: String?): Action {
        val actionBytes = Base64.decode(encodedActionBytes, Base64.DEFAULT)
        val actionBytesInputStream = ByteArrayInputStream(actionBytes)
        val objectInputStream = ObjectInputStream(actionBytesInputStream)
        val action: Action = objectInputStream.readObject() as Action
        objectInputStream.close()
        return action
    }

    fun readAll(): Map<String, Action> {
        val result = HashMap<String, Action>()
        val all = actionsPreferences.all
        for (key: String in all.keys) {
            result[key] = readActionFromEncodedBytes(all[key] as String?)
        }
        return result
    }

    fun clear() {
        actionsPreferences.edit().clear().apply()
    }
}
