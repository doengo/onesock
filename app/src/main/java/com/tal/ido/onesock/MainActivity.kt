package com.tal.ido.onesock

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.tal.ido.onesock.action.ActionsRepository
import com.tal.ido.onesock.action.ActionsStorer
import com.tal.ido.onesock.file.FilesDeleter
import com.tal.ido.onesock.file.browser.FileBrowser
import com.tal.ido.onesock.file.browser.FileBrowserGui
import com.tal.ido.onesock.file.sync.SyncBrowserModel
import com.tal.ido.onesock.file.sync.*
import com.tal.ido.onesock.file.sync.status.StatusRepository
import com.tal.ido.onesock.file.sync.status.StatusStorer
import kotlinx.android.synthetic.main.activity_main.*
import utils.activity.BackGoingActivity
import utils.general.LOG_TAG
import utils.general.setBaseDirectory
import java.io.File

const val ACTIONS_PREFERENCES = "ACTIONS_PREFERENCES"
const val STATUS_PREFERENCES = "STATUS_PREFERENCES"
class MainActivity : BackGoingActivity() {
    private lateinit var fileBrowser: FileBrowser

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(LOG_TAG, "Started app")
        setBaseDirectory(filesDir.absolutePath)

        setContentView(R.layout.activity_main)

        setup(filesDir)
    }

    override fun onBackPressed() {
        fileBrowser.onBack()
    }

    override fun goBack() {
        Log.d(LOG_TAG, "Exiting app")
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        fileBrowser.onActivityResult(requestCode, resultCode, data)
    }

    private fun setup(baseDirectory: File) {
        Log.d(LOG_TAG, "Started main activity setup")
        val actionsRepository = ActionsRepository(ActionsStorer(getSharedPreferences(ACTIONS_PREFERENCES, Context.MODE_PRIVATE)))
        val firebase = FirebaseFacade()
        val syncGui = SyncGui(syncDisplayer, this)
        val statusRepository = StatusRepository(StatusStorer(getSharedPreferences(STATUS_PREFERENCES, Context.MODE_PRIVATE)))
        val actionsSender = ActionsSender(firebase, statusRepository, syncGui)
        val remoteActionsExecutor = RemoteActionsExecutor(actionsRepository, statusRepository, firebase, syncGui)
        val filesSyncer = FilesSyncer(actionsRepository, firebase, syncGui, remoteActionsExecutor, actionsSender)
        val filesSyncTracker = FilesSyncTracker(actionsRepository, filesSyncer, statusRepository)
        val filesDeleter = FilesDeleter()
        val syncBrowserModel = SyncBrowserModel(filesSyncTracker, this, filesDeleter)

        fileBrowser = FileBrowser(baseDirectory,
                syncBrowserModel,
                FileBrowserGui(filesListView, pathTextView, pathScrollView, browserHeadlineTextView, uploadButton, newDirectoryButton),
                true,
                this)

        syncGui.register(fileBrowser)
        filesDeleter.register(fileBrowser)
        Log.d(LOG_TAG, "Main activity setup complete")
    }
}
