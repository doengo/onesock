package com.tal.ido.onesock.action

import com.tal.ido.onesock.action.exceptions.RepositoryBlockedException
import java.util.concurrent.atomic.AtomicBoolean

class ActionsRepository(private val actionsStorer: ActionsStorer) {
    private val blocked = AtomicBoolean(false)

    @Synchronized
    fun add(type: ActionType, relativePath: String) {
        addAction(Action(type, relativePath, System.currentTimeMillis()))
    }

    @Synchronized
    fun addAction(action: Action) {
        if (blocked.get()) {
            throw RepositoryBlockedException()
        }
        actionsStorer.writeAction(action)
    }

    @Synchronized
    fun get(relativePath: String): Action {
        return actionsStorer.readAction(relativePath)
    }

    @Synchronized
    fun getAll(): Map<String, Action> {
        return actionsStorer.readAll()
    }

    @Synchronized
    fun clear() {
        actionsStorer.clear()
    }

    @Synchronized
    fun getAllAndBlock(): Map<String, Action> {
        val result = getAll()
        block()
        return result
    }

    @Synchronized
    fun block() {
        blocked.set(true)
    }

    @Synchronized
    fun unBlock() {
        blocked.set(false)
    }
}
