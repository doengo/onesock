package com.tal.ido.onesock.action.exceptions

import java.lang.Exception

class RepositoryBlockedException : Exception()