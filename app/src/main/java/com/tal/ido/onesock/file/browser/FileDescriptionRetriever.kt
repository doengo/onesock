package com.tal.ido.onesock.file.browser

import java.io.File

interface FileDescriptionRetriever {
    fun getFileDescription(file: File): String
}