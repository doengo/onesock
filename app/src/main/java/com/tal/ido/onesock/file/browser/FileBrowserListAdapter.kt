package com.tal.ido.onesock.file.browser

import android.app.Activity
import android.graphics.Bitmap
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.tal.ido.onesock.R
import com.tal.ido.onesock.data.Cache
import com.tal.ido.onesock.file.navigator.DirectoryNavigator
import utils.general.setThumbnail
import java.util.*

class FileBrowserListAdapter(private val directoryNavigator: DirectoryNavigator, private val fileDescriptionRetriever: FileDescriptionRetriever, private val activity: Activity) : BaseAdapter() {
    private val currentFilesWithThumbnails: Cache<String, Bitmap> = Cache(100)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView

        val newView = view == null
        if (newView) {
            view = activity.layoutInflater.inflate(R.layout.file_item, null)
        }
        view!!

        val file = directoryNavigator.currentDirectory.listFiles()[position]

        val fileIcon = view.findViewById<ImageView>(R.id.fileIcon)
        val fileName = view.findViewById<TextView>(R.id.fileName)
        val fileMetadata = view.findViewById<TextView>(R.id.fileMetadata)
        val videoTopIcon = view.findViewById<ImageView>(R.id.videoTopIcon)

        val fromCache = currentFilesWithThumbnails.get(file.absolutePath)
        if (fromCache != null) {
            fileIcon.setImageBitmap(fromCache)
        } else {
            // Calculating a thumbnail is a heavy process we should avoid repeating, thus we do it only if we encounter a new file or a new view
            setThumbnail(file, fileIcon, videoTopIcon, activity, currentFilesWithThumbnails)
        }

        fileName.text = file.name

        val daysSinceLastModification = ((System.currentTimeMillis() - file.lastModified()) * 1.15740741 / 100000000).toInt()
        val lastModifiedString = when {
            daysSinceLastModification < 1 -> "modified today"
            daysSinceLastModification == 1 -> "modified yesterday"
            daysSinceLastModification < 30 -> "modified $daysSinceLastModification days ago"
            daysSinceLastModification < 365 -> "modified " + (daysSinceLastModification / 30) + " months ago"
            else -> "modified " + (daysSinceLastModification / 365) + " years ago"
        }

        val sizeKb = file.length() / 1024
        val sizeString = when {
            sizeKb == 0L -> "empty"
            sizeKb < 1024 -> "$sizeKb KB"
            else -> String.format(Locale.ENGLISH, "%.2f MB", sizeKb / 1024.0)
        }

        val statusString = fileDescriptionRetriever.getFileDescription(file)

        if (file.isDirectory) {
            val filesCountString = when {
                file.list().isEmpty() -> "empty"
                file.list().size == 1 -> "1 file"
                else -> "" + file.list().size + " files"
            }
            fileMetadata.text = "$filesCountString, $lastModifiedString"
        } else {
            if (statusString != "") {
                fileMetadata.text = "$sizeString, $lastModifiedString, $statusString"
            } else {
                fileMetadata.text = "$sizeString, $lastModifiedString"
            }
        }

        return view
    }

    override fun getItem(position: Int): Any? {
        return if (directoryNavigator.currentDirectory.list().size > position) {
            directoryNavigator.currentDirectory.list()[position]
        } else {
            null
        }
    }

    override fun getItemId(position: Int): Long {
        return if (directoryNavigator.currentDirectory.list().size > position) {
            directoryNavigator.currentDirectory.list()[position].hashCode().toLong()
        } else {
            0
        }
    }

    override fun getCount(): Int {
        return directoryNavigator.currentDirectory.listFiles().size
    }
}
