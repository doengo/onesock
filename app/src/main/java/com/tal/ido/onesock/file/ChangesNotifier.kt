package com.tal.ido.onesock.file

import utils.notify.Notified
import utils.notify.Notifier
import java.io.File

interface FileChangesNotified : Notified<File?>

open class FileChangesNotifier : Notifier<File?>()
