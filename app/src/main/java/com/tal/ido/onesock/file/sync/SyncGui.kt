package com.tal.ido.onesock.file.sync

import android.app.Activity
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.tal.ido.onesock.R
import com.tal.ido.onesock.file.FileChangesNotifier
import utils.general.LOG_TAG
import java.lang.Thread.sleep
import java.util.concurrent.atomic.AtomicBoolean

class SyncGui(private val syncDisplayer: ImageView, private val activity: Activity) : FileChangesNotifier() {
    private var inSession: AtomicBoolean = AtomicBoolean(true)

    @Synchronized
    fun onSessionStart() {
        inSession.set(true)
        Log.d(LOG_TAG, "Sync session started")
        displaySync()
        Thread(Runnable {
            while (inSession.get()) {
                sleep(500)
                notify(null)
            }
        }).start()
    }

    @Synchronized
    fun onSessionFailure() {
        inSession.set(false)
        Log.d(LOG_TAG, "Sync session failed")
        hideSync()
    }

    @Synchronized
    fun onSessionSuccess() {
        inSession.set(false)
        Log.d(LOG_TAG, "Sync session succeeded")
        hideSync()
    }

    @Synchronized
    fun onDownloadedFile(relativePath: String) {
        Log.d(LOG_TAG, "Downloaded file: $relativePath")
        notify(null)
    }

    @Synchronized
    fun onUploadedFile(relativePath: String) {
        Log.d(LOG_TAG, "Uploaded file: $relativePath")
        notify(null)
    }

    @Synchronized
    fun onDeleteFile(relativePath: String) {
        Log.d(LOG_TAG, "Deleted file: $relativePath")
        notify(null)
    }

    @Synchronized
    fun onUploadStart(relativePath: String) {
        Log.d(LOG_TAG, "Uploading file: $relativePath")
        notify(null)
    }

    @Synchronized
    fun onDownloadStart(relativePath: String) {
        Log.d(LOG_TAG, "Downloading file: $relativePath")
        notify(null)
    }

    private fun displaySync() {
        activity.runOnUiThread {
            syncDisplayer.visibility = View.VISIBLE
            val rotation = AnimationUtils.loadAnimation(activity, R.anim.rotate)
            rotation.fillAfter = true
            syncDisplayer.animation = rotation
        }
    }

    private fun hideSync() {
        activity.runOnUiThread {
            syncDisplayer.visibility = View.INVISIBLE
            syncDisplayer.clearAnimation()
        }
    }
}
