package com.tal.ido.onesock.file.sync

import android.util.Log
import com.google.firebase.storage.UploadTask
import com.tal.ido.onesock.action.Action
import com.tal.ido.onesock.action.ActionType
import com.tal.ido.onesock.file.sync.status.FileSyncStatus
import com.tal.ido.onesock.file.sync.status.FileSyncStatusType
import com.tal.ido.onesock.file.sync.status.StatusRepository
import utils.general.LOG_TAG
import utils.general.getAbsolutePath
import java.io.File

class ActionsSender(private val firebase: FirebaseFacade, private val statusRepository: StatusRepository, private val syncGui: SyncGui) {
    fun sendActionToServer(myAction: Action, uploadTasks: ArrayList<UploadTask>) {
        when (myAction.type) {
            ActionType.ADD -> {
                addFileToServer(myAction, uploadTasks)
            }
            ActionType.DELETE -> {
                deleteFileOnServer(myAction)
            }
        }
    }

    private fun addFileToServer(myAction: Action, uploadTasks: ArrayList<UploadTask>) {
        statusRepository.set(FileSyncStatus(FileSyncStatusType.UPLOADING), myAction.relativeFilePath)

        val uploadTask = firebase.uploadFile(myAction.relativeFilePath, getAbsolutePath(myAction.relativeFilePath))
        syncGui.onUploadStart(myAction.relativeFilePath)

        uploadTask.addOnSuccessListener {
            statusRepository.set(FileSyncStatus(FileSyncStatusType.SYNCED), myAction.relativeFilePath)
            syncGui.onUploadedFile(myAction.relativeFilePath)
            firebase.uploadAction(myAction)
        }.addOnFailureListener { e ->
            Log.e(LOG_TAG, "Failed to upload file ${myAction.relativeFilePath}", e)
            statusRepository.set(FileSyncStatus(FileSyncStatusType.TO_BE_ADDED), myAction.relativeFilePath)
        }.addOnProgressListener { taskSnapshot ->
            statusRepository.set(FileSyncStatus(FileSyncStatusType.UPLOADING, ((100.0 * taskSnapshot.bytesTransferred) / taskSnapshot.totalByteCount).toInt()), myAction.relativeFilePath)
        }

        uploadTasks.add(uploadTask)
    }

    private fun deleteFileOnServer(myAction: Action) {
        val actionUploadTask = firebase.uploadAction(myAction)

        actionUploadTask.addOnSuccessListener {
            statusRepository.set(FileSyncStatus(FileSyncStatusType.SYNCED), myAction.relativeFilePath)
            File(getAbsolutePath(myAction.relativeFilePath)).delete()
            syncGui.onDeleteFile(myAction.relativeFilePath)
        }
    }
}