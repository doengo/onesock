package com.tal.ido.onesock.file.sync.status

import java.io.Serializable

data class FileSyncStatus(val statusType: FileSyncStatusType, var progress: Int): Serializable {
    constructor(statusType: FileSyncStatusType) : this(statusType, 0)

    fun userRepresentation(): String {
        return when(statusType) {
            FileSyncStatusType.UPLOADING -> statusType.userRepresentation + " " + progress + "%"
            FileSyncStatusType.DOWNLOADING -> statusType.userRepresentation + " " + progress + "%"
            else -> statusType.userRepresentation
        }
    }
}