package com.tal.ido.onesock.file.sync

import com.tal.ido.onesock.action.ActionType
import com.tal.ido.onesock.action.ActionsRepository
import com.tal.ido.onesock.file.sync.status.FileSyncStatus
import com.tal.ido.onesock.file.sync.status.FileSyncStatusType
import com.tal.ido.onesock.file.sync.status.StatusRepository
import java.util.concurrent.atomic.AtomicBoolean

class FilesSyncTracker(private val actionsRepository: ActionsRepository, private val filesSyncer: FilesSyncer, private val statusRepository: StatusRepository) : SessionNotified {
    var inSession = AtomicBoolean(false)

    override fun onSessionStart() {
        inSession.set(true)
    }

    override fun onSessionEnd() {
        inSession.set(false)
    }

    init {
        filesSyncer.registerSessionNotified(this)
    }

    fun onNewFile(relativePath: String) {
        actionsRepository.add(ActionType.ADD, relativePath)
        statusRepository.set(FileSyncStatus(FileSyncStatusType.TO_BE_ADDED), relativePath)
    }

    fun onDeleteFile(relativePath: String) {
        actionsRepository.add(ActionType.DELETE, relativePath)
        statusRepository.set(FileSyncStatus(FileSyncStatusType.TO_BE_DELETED), relativePath)
    }

    fun sync() {
        filesSyncer.tryStartSyncSession()
    }

    fun getFileStatus(relativePath: String): FileSyncStatus {
        return statusRepository.get(relativePath)
    }
}
