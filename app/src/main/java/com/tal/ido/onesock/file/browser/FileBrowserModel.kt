package com.tal.ido.onesock.file.browser

import java.io.File

interface FileBrowserModel: FileDescriptionRetriever {
    fun canUpload(): Boolean
    fun legalFileName(name: String): Boolean
    fun onFileUploaded(file: File)
    fun canCreteNewDirectory(): Boolean
    fun onNewDirectory(file: File)
    fun onClickedFile(file: File)
    fun onLongClickedFile(file: File)
}