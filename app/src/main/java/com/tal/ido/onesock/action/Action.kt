package com.tal.ido.onesock.action

import java.io.Serializable

data class Action(var type: ActionType, var relativeFilePath: String, var epochTimeStamp: Long) : Serializable {
    constructor() : this(ActionType.ADD, "", 0)
}