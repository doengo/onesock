package com.tal.ido.onesock.file.navigator

import java.io.File

data class DirectoryNavigator(var baseDirectory: File, var currentDirectory: File)
