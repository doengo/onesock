package com.tal.ido.onesock.data

import java.util.*

class Cache<in T, K>(private val maxSize: Int) {
    private val cacheMap: MutableMap<T, K> = HashMap()
    private val random = Random()

    @Synchronized
    fun get(key: T): K? {
        return cacheMap[key]
    }

    @Synchronized
    fun put(key: T, value: K) {
        cacheMap[key] = value
        removeRandom()
    }

    @Synchronized
    fun contains(key: T): Boolean {
        return cacheMap.contains(key)
    }

    private fun removeRandom() {
        if (cacheMap.size > maxSize) {
            val keysAsArray = ArrayList(cacheMap.keys)
            cacheMap.remove(keysAsArray[random.nextInt(keysAsArray.size)])
        }
    }
}