package com.tal.ido.onesock.file.sync.status

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.util.Base64
import com.tal.ido.onesock.file.sync.status.exceptions.NoSyncStatusForFile
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

class StatusStorer(private val statusPreferences: SharedPreferences) {
    @SuppressLint("ApplySharedPref")
    fun store(status: FileSyncStatus, relativePath: String) {
        val statusBytesOutputStream = ByteArrayOutputStream()
        val objectOutputStream = ObjectOutputStream(statusBytesOutputStream)
        objectOutputStream.writeObject(status)
        objectOutputStream.close()
        val statusBytes = statusBytesOutputStream.toByteArray()
        val encodedStatusBytes = Base64.encodeToString(statusBytes, Base64.DEFAULT)
        statusPreferences.edit().putString(relativePath, encodedStatusBytes).commit()
        read(relativePath)
    }

    fun read(relativePath: String): FileSyncStatus {
        val encodedStatusBytes = statusPreferences.getString(relativePath, "none")
        if (encodedStatusBytes == "none") {
            throw NoSyncStatusForFile(relativePath)
        }
        return readStatusFromEncodedBytes(encodedStatusBytes)
    }

    private fun readStatusFromEncodedBytes(encodedStatusBytes: String?): FileSyncStatus {
        val statusBytes = Base64.decode(encodedStatusBytes, Base64.DEFAULT)
        val statusBytesInputStream = ByteArrayInputStream(statusBytes)
        val objectInputStream = ObjectInputStream(statusBytesInputStream)
        val status: FileSyncStatus = objectInputStream.readObject() as FileSyncStatus
        objectInputStream.close()
        return status
    }

    fun readAll(): Map<String, FileSyncStatus> {
        val result = HashMap<String, FileSyncStatus>()
        val all = statusPreferences.all
        for (key: String in all.keys) {
            result[key] = readStatusFromEncodedBytes(all[key] as String?)
        }
        return result
    }

    fun clear() {
        statusPreferences.edit().clear().apply()
    }
}
