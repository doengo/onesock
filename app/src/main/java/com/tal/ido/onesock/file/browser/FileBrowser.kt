package com.tal.ido.onesock.file.browser

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.Toast
import com.tal.ido.onesock.file.FileChangesNotified
import com.tal.ido.onesock.file.choosing.ChooseFileActivity
import com.tal.ido.onesock.file.navigator.DirectoryNavigator
import com.tal.ido.onesock.file.navigator.DirectoryNavigatorCreator
import utils.activity.BackGoingActivity
import java.io.File


class FileBrowser(baseDirectory: File, private val fileBrowserModel: FileBrowserModel, private val fileBrowserGui: FileBrowserGui, changeable: Boolean, private val activity: BackGoingActivity, headline: String = baseDirectory.name) : FileChangesNotified {
    private val GET_CONTENT_RETURN_CODE: Int = 4
    private val REQUEST_PERMISSION_RETURN_CODE: Int = 6

    private val directoryNavigator: DirectoryNavigator = DirectoryNavigatorCreator().createFileTree(baseDirectory)

    init {
        fileBrowserGui.filesListView.adapter = FileBrowserListAdapter(directoryNavigator, fileBrowserModel, activity)

        if (changeable) {
            fileBrowserGui.uploadButton.setOnClickListener {
                if (fileBrowserModel.canUpload()) {
                    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_PERMISSION_RETURN_CODE)
                    } else {
                        val chooseFileIntent = Intent(activity, ChooseFileActivity::class.java)
                        activity.startActivityForResult(chooseFileIntent, GET_CONTENT_RETURN_CODE)
                    }
                }
            }

            fileBrowserGui.newDirectoryButton.setOnClickListener {
                if (fileBrowserModel.canCreteNewDirectory()) {
                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle("Choose a name for the new directory:")

                    val inputText = EditText(activity)

                    builder.setView(inputText)

                    builder.setPositiveButton("Create") { _, _ -> createNewDirectory(inputText.text.toString()) }
                    builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }

                    builder.show()
                }
            }
        } else {
            fileBrowserGui.uploadButton.visibility = View.GONE
            fileBrowserGui.newDirectoryButton.visibility = View.GONE
        }

        fileBrowserGui.filesListView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val chosenFile = directoryNavigator.currentDirectory.listFiles()[position]
            if (chosenFile.isDirectory) {
                replaceBaseDirectory(chosenFile)
            } else {
                fileBrowserModel.onClickedFile(chosenFile)
                onFilesChanged()
            }
        }

        fileBrowserGui.filesListView.onItemLongClickListener = AdapterView.OnItemLongClickListener { _, _, pos, _ ->
            val longClickedFile = directoryNavigator.currentDirectory.listFiles()[pos]
            fileBrowserModel.onLongClickedFile(longClickedFile)
            onFilesChanged()
            true
        }

        fileBrowserGui.pathTextView.text = ""

        fileBrowserGui.browserHeadlineTextView.text = headline
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            GET_CONTENT_RETURN_CODE -> if (resultCode == Activity.RESULT_OK) {
                val chosenFilePath = data!!.data!!.path
                uploadFile(chosenFilePath)
            }
        }
    }

    fun onBack() {
        if (directoryNavigator.currentDirectory == directoryNavigator.baseDirectory) {
            activity.goBack()
        } else {
            replaceBaseDirectory(directoryNavigator.currentDirectory.parentFile)
        }
    }

    private fun createNewDirectory(directoryName: String) {
        when {
            directoryNavigator.currentDirectory.list().contains(directoryName) -> Toast.makeText(activity, "Directory already exists", Toast.LENGTH_SHORT).show()
            directoryName == "" -> Toast.makeText(activity, "Directory name can't be empty", Toast.LENGTH_SHORT).show()
            else -> {
                val newDirectoryFile = File(directoryNavigator.currentDirectory.absolutePath + "/" + directoryName)
                newDirectoryFile.mkdir()
                fileBrowserModel.onNewDirectory(newDirectoryFile)
                onFilesChanged()
            }
        }
    }

    private fun uploadFile(chosenFilePath: String?) {
        val chosenFile = File(chosenFilePath)
        val destinationFile = File(directoryNavigator.currentDirectory.path + "/" + chosenFile.name)

        if (!fileBrowserModel.legalFileName(chosenFile.name)) {
            return
        }

        val resultFile = chosenFile.copyTo(destinationFile, overwrite = true)

        fileBrowserModel.onFileUploaded(resultFile)

        onFilesChanged()
    }

    private fun replaceBaseDirectory(newBaseDirectory: File) {
        fileBrowserGui.pathTextView.text = when (newBaseDirectory) {
            directoryNavigator.currentDirectory.parentFile -> fileBrowserGui.pathTextView.text.substring(0, fileBrowserGui.pathTextView.text.length - directoryNavigator.currentDirectory.name.length - 3)
            else -> (fileBrowserGui.pathTextView.text.toString() + newBaseDirectory.name + " > ")
        }
        directoryNavigator.currentDirectory = newBaseDirectory
        onFilesChanged()
        fileBrowserGui.pathScrollView.post {
            fileBrowserGui.pathScrollView.fullScroll(View.FOCUS_RIGHT)
        }
    }

    override fun onNotify(argument: File?) {
        onFilesChanged()
    }

    private fun onFilesChanged() {
        activity.runOnUiThread {
            (fileBrowserGui.filesListView.adapter as BaseAdapter).notifyDataSetChanged()
        }
    }
}
