package com.tal.ido.onesock.file.sync

import android.util.Log
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.UploadTask
import utils.general.LOG_TAG
import com.tal.ido.onesock.action.Action
import com.tal.ido.onesock.action.ActionsRepository
import com.tal.ido.onesock.file.sync.exceptions.ResolvingException
import utils.general.fromEscapedFirebaseDatabasePath
import utils.general.toEscapedFirebaseDatabasePath
import java.lang.Thread.sleep
import java.util.concurrent.atomic.AtomicBoolean

class FilesSyncer(private val actionsRepository: ActionsRepository, private val firebase: FirebaseFacade, private val syncGui: SyncGui, private val remoteActionsExecutor: RemoteActionsExecutor, private val actionsSender: ActionsSender) {
    private var inSession: AtomicBoolean = AtomicBoolean(false)
    private val sessionNotified = ArrayList<SessionNotified>()

    init {
        firebase.registerSyncer(this)
    }

    @Synchronized
    fun tryStartSyncSession() {
        if (inSession.getAndSet(true)) {
            return
        }

        Thread(Runnable {
            startSyncSession()
        }).start()
    }

    private fun startSyncSession() {
        try {
            notifySessionStarted()

            val myActions: Map<String, Action> = actionsRepository.getAll()
            val databaseActions = firebase.takeDatabaseActionsSnapshot()

            val uploadTasks = ArrayList<UploadTask>()
            val downloadTasks = ArrayList<FileDownloadTask>()

            resolveActions(myActions, databaseActions, uploadTasks, downloadTasks)

            waitForTasks(uploadTasks, downloadTasks)

            syncGui.onSessionSuccess()
        } catch (e: ResolvingException) {
            syncGui.onSessionFailure()
        } finally {
            notifySessionEnded()
            inSession.set(false)
        }
    }

    fun registerSessionNotified(notified: FilesSyncTracker) {
        sessionNotified.add(notified)
    }

    private fun notifySessionStarted() {
        syncGui.onSessionStart()
        sessionNotified.forEach { notified ->
            notified.onSessionStart()
        }
    }

    private fun notifySessionEnded() {
        sessionNotified.forEach { notified ->
            notified.onSessionEnd()
        }
    }

    private fun resolveActions(myActions: Map<String, Action>, currentDatabaseActions: Map<String, Action>, uploadTasks: ArrayList<UploadTask>, downloadTasks: ArrayList<FileDownloadTask>) {
        for (remotePath: String in currentDatabaseActions.keys) {
            val databaseAction = currentDatabaseActions.getValue(remotePath)
            val localPath = fromEscapedFirebaseDatabasePath(remotePath)
            val myAction = myActions[localPath]

            if (myAction == null || databaseAction.epochTimeStamp > myAction.epochTimeStamp) {
                if (myAction != null) {
                    Log.d(LOG_TAG, "Executing new action for file $localPath: ${databaseAction.type.name} from server")
                } else {
                    Log.d(LOG_TAG, "No local action for file $localPath: ${databaseAction.type.name}, executing action from server")
                }
                remoteActionsExecutor.executeActionFromServer(databaseAction, downloadTasks)
            }
        }

        for (localPath: String in myActions.keys) {
            val myAction = myActions.getValue(localPath)
            val remotePath = toEscapedFirebaseDatabasePath(localPath)
            val databaseAction = currentDatabaseActions[toEscapedFirebaseDatabasePath(localPath)]

            if (databaseAction == null || myAction.epochTimeStamp > databaseAction.epochTimeStamp) {
                if (databaseAction != null) {
                    Log.d(LOG_TAG, "Sending new action for file $remotePath: ${myAction.type.name} to server")
                } else {
                    Log.d(LOG_TAG, "No remote action for file $remotePath: ${myAction.type.name}, sending local one to server")
                }
                actionsSender.sendActionToServer(myAction, uploadTasks)
            }
        }
    }

    private fun waitForTasks(uploadTasks: ArrayList<UploadTask>, downloadTasks: ArrayList<FileDownloadTask>) {
        while (!finished(uploadTasks, downloadTasks)) {
            sleep(100)
        }
    }

    private fun finished(uploadTasks: ArrayList<UploadTask>, downloadTasks: ArrayList<FileDownloadTask>): Boolean {
        return downloadTasks.all { downloadTask -> downloadTask.isComplete } &&
                uploadTasks.all { uploadTask -> uploadTask.isComplete }
    }
}