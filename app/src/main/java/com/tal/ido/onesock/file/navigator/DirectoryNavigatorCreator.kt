package com.tal.ido.onesock.file.navigator

import java.io.File

class DirectoryNavigatorCreator {
    fun createFileTree(filesDir: File): DirectoryNavigator {
        return DirectoryNavigator(filesDir, filesDir)
    }
}