package com.tal.ido.onesock.file.sync.status

import com.tal.ido.onesock.file.sync.status.exceptions.NoSyncStatusForFile

class StatusRepository(private val statusStorer: StatusStorer) {
    @Synchronized
    fun set(status: FileSyncStatus, relativePath: String) {
        statusStorer.store(status, relativePath)
    }

    @Synchronized
    fun get(relativePath: String): FileSyncStatus {
        return try {
            statusStorer.read(relativePath)
        } catch (e: NoSyncStatusForFile) {
            FileSyncStatus(FileSyncStatusType.UNTRACKED)
        }
    }

    @Synchronized
    fun getAll(): Map<String, FileSyncStatus> {
        return statusStorer.readAll()
    }

    @Synchronized
    fun clear() {
        statusStorer.clear()
    }
}