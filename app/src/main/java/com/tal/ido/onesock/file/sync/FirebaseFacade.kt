package com.tal.ido.onesock.file.sync

import android.net.Uri
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.tal.ido.onesock.action.Action
import utils.general.toEscapedFirebaseDatabasePath
import java.io.File

class FirebaseFacade {
    var databaseActions = HashMap<String, Action>()
    val syncers = ArrayList<FilesSyncer>()

    private val databaseReference: DatabaseReference = FirebaseDatabase.getInstance().getReference("syncer/1337")
    private val storageReference = FirebaseStorage.getInstance().getReference("storage/1337")

    init {
        FirebaseStorage.getInstance().maxUploadRetryTimeMillis = 5000
        FirebaseStorage.getInstance().maxDownloadRetryTimeMillis = 5000
        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val newDatabaseActions = HashMap<String, Action>()
                for (snapChild in snapshot.children) {
                    newDatabaseActions[snapChild.key!!] = snapChild.getValue(Action::class.java)!!
                }
                databaseActions = newDatabaseActions
                for (syncer in syncers) {
                    syncer.tryStartSyncSession()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

    fun registerSyncer(filesSyncer: FilesSyncer) {
        syncers.add(filesSyncer)
    }

    fun takeDatabaseActionsSnapshot(): Map<String, Action> {
        return databaseActions.clone() as Map<String, Action>
    }

    fun uploadAction(action: Action): Task<Void> {
        return databaseReference.child(toEscapedFirebaseDatabasePath(action.relativeFilePath)).setValue(action)
    }

    fun downloadFile(serverPath: String, localPath: String): FileDownloadTask {
        return storageReference.child(serverPath).getFile(File(localPath))
    }

    fun uploadFile(serverPath: String, localPath: String): UploadTask {
        return storageReference.child(serverPath).putFile(Uri.fromFile(File(localPath)))
    }
}