package com.tal.ido.onesock.file.sync.exceptions

import java.lang.Exception

class ResolvingException: Exception()
