package com.tal.ido.onesock.file.sync.status

enum class FileSyncStatusType(val userRepresentation: kotlin.String) {
    SYNCED("synced"),
    TO_BE_ADDED("set to upload"),
    TO_BE_DELETED("set to delete"),
    DOWNLOADING("downloading"),
    UPLOADING("uploading"),
    UNTRACKED("not tracked")
}
