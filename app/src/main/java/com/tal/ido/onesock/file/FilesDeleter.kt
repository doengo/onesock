package com.tal.ido.onesock.file

import java.io.File
import java.lang.Thread.sleep

class FilesDeleter : FileChangesNotifier() {
    fun deleteWhenEmpty(fileToDelete: File) {
        if (!fileToDelete.isDirectory) {
            fileToDelete.delete()
        }
        Thread {
            while (fileToDelete.list().isNotEmpty()) {
                sleep(100)
            }
            fileToDelete.delete()
            notify(fileToDelete)
        }.start()
    }
}