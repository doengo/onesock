package com.tal.ido.onesock.file.sync

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.v4.content.FileProvider
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.Toast
import com.tal.ido.onesock.file.FilesDeleter
import com.tal.ido.onesock.file.browser.FileBrowserModel
import com.tal.ido.onesock.file.sync.status.FileSyncStatusType
import utils.general.LOG_TAG
import utils.general.escapableFileName
import utils.general.getRelativePath
import java.io.File


class SyncBrowserModel(private val filesSyncTracker: FilesSyncTracker, private val activity: Activity, private val filesDeleter: FilesDeleter) : FileBrowserModel {
    override fun getFileDescription(file: File): String {
        return filesSyncTracker.getFileStatus(getRelativePath(file.absolutePath)).userRepresentation()
    }

    override fun canUpload(): Boolean {
        return if (filesSyncTracker.inSession.get()) {
            Toast.makeText(activity, "Can't upload files while in sync session", Toast.LENGTH_SHORT).show()
            false
        } else true
    }

    override fun legalFileName(name: String): Boolean {
        if (!escapableFileName(name)) {
            Toast.makeText(activity, "File names must not contain '%', '^', '~', '{' or '}'", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    override fun onFileUploaded(file: File) {
        filesSyncTracker.onNewFile(getRelativePath(file.absolutePath))
        filesSyncTracker.sync()
    }

    override fun canCreteNewDirectory(): Boolean {
        return true
    }

    override fun onNewDirectory(file: File) {}

    override fun onClickedFile(file: File) {
        val mime = MimeTypeMap.getSingleton()
        val ext = file.name.substring(file.name.lastIndexOf(".") + 1)
        val type = mime.getMimeTypeFromExtension(ext)
        try {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                val contentUri = FileProvider.getUriForFile(activity, "com.tal.ido.onesock.fileProvider", file)
                intent.setDataAndType(contentUri, type)
            } else {
                intent.setDataAndType(Uri.fromFile(file), type)
            }
            activity.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Log.w(LOG_TAG, "No activity found to open attachment: $file", e)
        }
    }

    override fun onLongClickedFile(file: File) {
        if (filesSyncTracker.inSession.get()) {
            Toast.makeText(activity, "Can't delete files while in sync session", Toast.LENGTH_SHORT).show()
        } else {
            deleteFile(file)
        }
    }

    private fun deleteFile(fileToDelete: File, quiet: Boolean = false) {
        if (filesSyncTracker.getFileStatus(getRelativePath(fileToDelete.absolutePath)).statusType == FileSyncStatusType.TO_BE_DELETED) {
            if (!quiet) Toast.makeText(activity, fileToDelete.path + " is already set to be deleted", Toast.LENGTH_SHORT).show()
        } else if (fileToDelete.isDirectory) {
            if (!quiet) Toast.makeText(activity, "Deleting directory ${fileToDelete.name}", Toast.LENGTH_SHORT).show()

            for (subFile in fileToDelete.listFiles()) {
                deleteFile(subFile, true)
            }

            filesDeleter.deleteWhenEmpty(fileToDelete)
        } else {
            if (!quiet) Toast.makeText(activity, "Deleting ${fileToDelete.name}", Toast.LENGTH_SHORT).show()
            filesSyncTracker.onDeleteFile(getRelativePath(fileToDelete.absolutePath))
        }
        filesSyncTracker.sync()
    }
}