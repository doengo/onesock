package com.tal.ido.onesock.file.sync

interface SessionNotified {
    fun onSessionStart()
    fun onSessionEnd()
}
