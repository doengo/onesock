package com.tal.ido.onesock.file.choosing

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import com.tal.ido.onesock.R
import com.tal.ido.onesock.file.browser.FileBrowser
import com.tal.ido.onesock.file.browser.FileBrowserGui
import kotlinx.android.synthetic.main.activity_main.*
import utils.activity.BackGoingActivity
import utils.general.LOG_TAG
import java.io.File

class ChooseFileActivity : BackGoingActivity() {

    private lateinit var fileBrowser: FileBrowser

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_choose_file)

        setup(File("/storage/emulated/0"))
    }

    override fun onBackPressed() {
        fileBrowser.onBack()
    }

    override fun goBack() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        fileBrowser.onActivityResult(requestCode, resultCode, data)
    }

    fun fileChosen(file: File) {
        val result = Intent()
        result.data = Uri.parse(file.absolutePath)
        setResult(RESULT_OK, result)
        finish()
    }

    private fun setup(baseDirectory: File) {
        Log.d(LOG_TAG, "Started choose file activity setup")
        val chooseBrowserModel = ChooseBrowserModel(this)

        fileBrowser = FileBrowser(baseDirectory,
                chooseBrowserModel,
                FileBrowserGui(filesListView, pathTextView, pathScrollView, browserHeadlineTextView, uploadButton, newDirectoryButton),
                true,
                this,
                "Choose a file")

        Log.d(LOG_TAG, "Choose file activity setup complete")
    }
}
