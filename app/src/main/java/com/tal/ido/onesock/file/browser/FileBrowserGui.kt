package com.tal.ido.onesock.file.browser

import android.widget.HorizontalScrollView
import android.widget.ImageButton
import android.widget.ListView
import android.widget.TextView

data class FileBrowserGui(val filesListView: ListView, val pathTextView: TextView, val pathScrollView: HorizontalScrollView, val browserHeadlineTextView: TextView, val uploadButton: ImageButton, val newDirectoryButton: ImageButton)