package com.tal.ido.onesock.file.choosing

import android.widget.Toast
import com.tal.ido.onesock.file.browser.FileBrowserModel
import java.io.File

class ChooseBrowserModel(private val activity: ChooseFileActivity) : FileBrowserModel {
    override fun canUpload(): Boolean {
        return false
    }

    override fun legalFileName(name: String): Boolean {
        return false
    }

    override fun onFileUploaded(file: File) {
    }

    override fun canCreteNewDirectory(): Boolean {
        return false
    }

    override fun onNewDirectory(file: File) {
    }

    override fun onClickedFile(file: File) {
        activity.fileChosen(file)
    }

    override fun onLongClickedFile(file: File) {
        Toast.makeText(activity, file.absolutePath, Toast.LENGTH_SHORT).show()
    }

    override fun getFileDescription(file: File): String {
        return ""
    }
}
